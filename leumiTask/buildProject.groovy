pipeline{
	
	agent none
	 stages{
		   stage('mvn') {
				agent { node { label 'master' } }
				steps {  
			           dir("C:/workspace/devopscourse/leumiTask"){
							script{				
									env.JAVA_HOME="${tool 'jdk1.8'}"
									env.PATH="${env.JAVA_HOME}/bin:${env.PATH}"
                     				bat "mvn install"
							}
			  		   }
			     
			 	}
			
		
		  }
	
	}
}